<?php

include "connection.php";
$id = $_GET['updateid'];
$sql = "SELECT * FROM `users` where id = $id";
$res = mysqli_query($con, $sql);
$row = mysqli_fetch_assoc($res);
$firstname = $row['firstname'];
$lastname = $row['lastname'];
$birthday = $row['birthday'];
$nt_tel = $row['nt_tel'];
$gender = $row['gender'];
$work_status = $row['work_status'];
$g_martesore = $row['g_martesore'];
$vendlindja = $row['vendlindja'];



if (isset($_POST["submit"])) {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $birthday = $_POST['birthday'];
    $nt_tel = $_POST['nt_tel'];
    $gender = $_POST['gender'];
    $work_status = $_POST['work_status'];
    $g_martesore = $_POST['g_martesore'];
    $vendlindja = $_POST['vendlindja'];

    $sql = "UPDATE `users` SET `id`='$id',`firstname`='$firstname',`lastname`='$lastname',`birthday`='$birthday',`nt_tel`='$nt_tel',`gender`='$gender',`work_status`='$work_status',`g_martesore`='$g_martesore',`vendlindja`='$vendlindja' WHERE id = $id";
    $result = mysqli_query($con, $sql);
    if ($result) {
        echo "Data update Successfully";
    }
    // mysqli_query($link, "INSERT INTO `users`(`firstname`, `lastname`, `birthday`, `nt_tel`, `gender`, `work_status`, `g_martesore`, `vendlindja`) VALUES ('$_POST[firstname]','$_POST[lastname]','$_POST[birthday]','$_POST[nt_tel]','$_POST[gender]','$_POST[work_status]','$_POST[g_martesore]','$_POST[vendlindja]')");
    // echo "Record Added Successfully";
    header('location:display.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Person CMS</title>
</head>

<body>
    <div class="container">
        <h2>Update Form</h2>
        <form action="" name="form1" method="POST">
            <div class="form-group">
                <label>Emri:</label>
                <input type="text" class="form-control" id="firstname" placeholder="Vendosni Emrin" name="firstname"
                    autocomplete="off" value=<?php echo $firstname ?>>
            </div>
            <div class="form-group">
                <label>Mbiemri:</label>
                <input type="text" class="form-control" id="lastname" placeholder="Vendosni Mbiemrin" name="lastname"
                    autocomplete="off" value="<?php echo $lastname ?>">
            </div>
            <div class="form-group">
                <label>Datlindja:</label>
                <input type="date" class="form-control" id="birthday" placeholder="Ditlindja" name="birthday"
                    autocomplete="off" value="<?php echo $birthday ?>">
            </div>
            <div class="form-group">
                <label>Nr_Tel:</label>
                <input type="phone" class="form-control" id="nt_tel" placeholder="Vendosni Numrin E Telefonit"
                    name="nt_tel" autocomplete="off" value="<?php echo $nt_tel ?>">
            </div>
            <div class="form-group">
                <label>Gjinia:</label>
                <input type="radio" id="gender" name="gender" value="M">
                <label>M</label><br>
                <input type="radio" id="gender" name="gender" value="F">
                <label>F</label><br>
            </div>
            <div class="form-group">
                <label>Statusi ne pune:</label>
                <label><input type="checkbox" name="work_status" autocomplete="off" value="<?php echo $work_status ?>">
                    Po</label>
                <label><input type="checkbox" name="work_status" autocomplete="off" value="<?php echo $work_status ?>">
                    Jo</label>
            </div>
            <div class="form-group">
                <label>Statusi Martesor:</label>
                <select name="g_martesore" id="g_martesore" autocomplete="off" value="<?php echo $g_martesore ?>">
                    <option value="imartuar">I Martuar</option>
                    <option value="indare">I Ndare</option>
                    <option value="beqar">Beqar</option>
                    <option value="ive">I/E Ve</option>
                </select>
            </div>
            <div class="form-group">
                <label>Vendlindja:</label>
                <input type="text" class="form-control" id="vendlindja" placeholder="Vendos Vendlindjen"
                    name="vendlindja" autocomplete="off" value="<?php echo $vendlindja ?>">
            </div>
            <button type="submit" name="submit" class="btn btn-warning">Update</button>
        </form>
    </div>
</body>

</html>