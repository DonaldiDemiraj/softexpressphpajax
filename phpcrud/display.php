<?php
include "connection.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Person CMS</title>
</head>

<body>
    <div class="container">
        <button class="btn btn-primary btn-lg" style="margin-top: 50px;"><a href="./user.php"
                style="color: #fff; text-decoration: none;">Add</a></button><br></br>
        <div class="card-body">
            <div class="row">
                <div class="col-md-7">
                    <form action="" method="GET">
                        <div class="input-group mb-3">
                            <input type="text" require name="search" value="<?php if (isset($_GET['search'])) {
                                                                                echo $_GET['search'];
                                                                            } ?>" class="form-control"
                                placeholder="Search Data">
                            <button type="submit" class="bn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <td>#</td>
                    <td>Emri</td>
                    <td>Mbiemri</td>
                    <td>Datlindja</td>
                    <td>Nr_tel</td>
                    <td>Gjinia</td>
                    <td>Statusi i Punes</td>
                    <td>Statusi martesor</td>
                    <td>Vendlindja</td>
                    <td>Veprime</td>
                </tr>
                <?php

                $sql =  "SELECT * FROM  `users` ";
                $res = mysqli_query($con, $sql);
                if ($res) {
                    while ($row = mysqli_fetch_assoc($res)) {
                        $id = $row['id'];
                        $firstname = $row['firstname'];
                        $lastname = $row['lastname'];
                        $birthday = $row['birthday'];
                        $nt_tel = $row['nt_tel'];
                        $gender = $row['gender'];
                        $work_status = $row['work_status'];
                        $g_martesore = $row['g_martesore'];
                        $vendlindja = $row['vendlindja'];
                ?>
                <?php

                        if (isset($_GET['search'])) {
                            $filter = $_GET['search'];
                            $sql =  "SELECT * FROM  `users` WHERE CONCAT (firstname,lastname,birthday,nt_tel,gander,work_status,g_martesore,vendlindja) LIKE '%$filter%' ";
                            $query_run = mysqli_query($con, $sql);

                            if (mysqli_num_rows($query_run) > 0) {
                                foreach ($query_run as $item) {
                        ?>
                <tr>
                    <td><?= $item['firstname'] ?></td>
                    <td><?= $item['lastname'] ?></td>
                    <td><?= $item['birthday'] ?></td>
                    <td><?= $item['nt_tel'] ?></td>
                    <td><?= $item['gender'] ?></td>
                    <td><?= $item['work_status'] ?></td>
                    <td><?= $item['g_martesore'] ?></td>
                    <td><?= $item['vendlindja'] ?></td>
                </tr>
                <?php
                                }
                            } else {
                                ?>

                <tr>
                    <td>No record found</td>
                </tr>
                <?php

                            }
                        }

                        ?>
            </thead>
            <tbody>
                <?php

                        echo '<tr>

                    <th scope="row">' . $id . '</th>
                    <td>' . $firstname . '</td>
                    <td>' . $lastname . '</td>
                    <td>' . $birthday . '</td>
                    <td>' . $nt_tel . '</td>
                    <td>' . $gender . '</td>
                    <td>' . $work_status . '</td>
                    <td>' . $g_martesore . ' </td>
                    <td>' . $vendlindja . '</td>
                    <td><button class="btn btn-warning"><a style="color: #fff; text-decoration: none;"><a href="update.php?updateid=' . $id . '">Update</a></button>
                    <td><button class="btn btn-danger"><a style="color: #fff; text-decoration: none;"><a href="delete.php?deleteid=' . $id . '">
                    Delete</a></button>
                </tr>';
                    }
                }


        ?>

            </tbody>
        </table>
    </div>
</body>

</html>